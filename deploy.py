from flask import Flask
from flask import render_template, url_for, redirect, request
import requests
import json
import sys

current_policy_ID = ""

def fdm_login():
    
    url = "https://fmcrestapisandbox.cisco.com/api/fmc_platform/v1/auth/generatetoken"

    headers = {
        'Content-Type': "application/json",
        'cache-control': "no-cache",
        }

    response = requests.request("POST", url, auth=('shruthi63', 'XU8GDYZ8'), headers=headers, verify=False)

    auth_token = response.headers['X-auth-access-token']
    refresh_token = response.headers['X-auth-refresh-token']
    return auth_token, refresh_token
    

def create(auth_token, refresh_token, policyType):
    print(auth_token)
    print(refresh_token)
    print(policyType)
    
    url = "https://fmcrestapisandbox.cisco.com/api/fmc_config/v1/domain/e276abec-e0f2-11e3-8169-6d9ed49b625f/policy/accesspolicies"

    post_data = {
          "type": "AccessPolicy",
          "name": policyType,
          "description": policyType,
          "defaultAction": {
            "intrusionPolicy": {
              "id": "d224e29c-6c27-11e0-ac9d-988fc3da9be6",
              "type": "IntrusionPolicy"
            },
            "type": "AccessPolicyDefaultAction",
            "logBegin": "False",
            "logEnd": "False",
            "sendEventsToFMC": "False",
            "action": "BLOCK"
          }
        }
    
    headers = {
        'Content-Type': "application/json",
        'X-auth-access-token':auth_token,
        'X-auth-refresh-token':refresh_token
    }


    response = requests.post(url, data=json.dumps(post_data), headers=headers, verify=False)
   
    print(response)
    print(response.headers)

def delete_policy(auth_token, refresh_token, policy_id):
    
    url = "https://fmcrestapisandbox.cisco.com/api/fmc_config/v1/domain/e276abec-e0f2-11e3-8169-6d9ed49b625f/policy/accesspolicies/" + policy_id

    headers = {
        'Content-Type': "application/json",
        'X-auth-access-token':auth_token,
        'X-auth-refresh-token':refresh_token
    }
    
    response = requests.delete(url, headers=headers, verify=False)

    print(response.text)

def create_url_object(name,desc,url):
    auth_token=fdm_login()
    #url = "https://192.168.137.222/api/fdm/v2/object/urls"
    payload = "{\r\n  \"name\": \" + name + \",\r\n  \"description\": \"Search\",\r\n  \"url\": \" + urls +\",\r\n  \"type\": \"urlobject\"\r\n}"
    headers = {
        'Content-Type': "application/json",
        'Authorization': "Bearer " + auth_token,
        'cache-control': "no-cache",
        }
    response = requests.request("POST", url, data=payload, headers=headers, verify=False)

    print(response.text)

def access_get(auth_token, refresh_token):
    
    url = "https://fmcrestapisandbox.cisco.com/api/fmc_config/v1/domain/e276abec-e0f2-11e3-8169-6d9ed49b625f/policy/accesspolicies?limit=200"
    
    
    headers = {
        'Content-Type': "application/json",
        'X-auth-access-token':auth_token,
        'X-auth-refresh-token':refresh_token
    }
    
    current_policy_name = ""
    current_policy_id = ""

    response = requests.get(url, headers=headers, verify=False)
    policies = json.loads(response.text)['items']
    for i in policies:
        if i['name'] == "Strict Policy" or i['name'] == "Exam Policy" or i['name'] == "Unrestricted Policy":
            current_policy_name = i['name']
            current_policy_id = i['id']
        if i['name'] == "shruthitest":
            print(i)
            
    return current_policy_name, current_policy_id
    
    

app = Flask(__name__)

@app.route('/')
def index():
    return render_template("index.html")


@app.route('/tasks')
def tasks():
    return render_template("blank.html")

@app.route('/class_stats')
def class_stats():
    return render_template("charts.html")

@app.route('/policy_assign', methods = ['GET', 'POST'])
def policy_assign():
    auth_token, refresh_token = fdm_login()
    currentPolicy, currentID = access_get(auth_token, refresh_token)
    select = request.form.get('change_policy')
    if select:
        delete_policy(auth_token, refresh_token, currentID)
        create(auth_token, refresh_token, select)
        currentPolicy, currentID = access_get(auth_token, refresh_token)
    return render_template("tables.html", currentPolicy = currentPolicy)

@app.route('/policy_assign1', methods = ['GET', 'POST'])
def update_policy():
    print("HELLO")
    '''auth_token, refresh_token = fdm_login()
    select = request.form.get('change_policy')
    print(select)
    create(auth_token, refresh_token, select)
    access_get(auth_token, refresh_token)
    currentPolicy = "Hello"
    return render_template("tables.html", currentPolicy = currentPolicy)'''
