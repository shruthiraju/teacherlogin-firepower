Instructions to run:

1. Install dependencies in requirements.txt
    **pip install -r requirements.txt**
2. Specify flask main file (Windows)        
    **set FLASK_APP=deploy.py**
3. Run (Windows)                            
    **flask run**